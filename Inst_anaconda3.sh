#!/bin/bash

#
# Script to install anaconda3
# version:
#
################################################################################

# Install GUI dependencies
sudo apt install -y libgl1-mesa-glx libegl1-mesa libxrandr2 libxrandr2 libxss1 \
libxcursor1 libxcomposite1 libasound2 libxi6 libxtst6

# Descargar paquete
wget https://www.anaconda.com/download/#linux

# Verifica integridad
sha256sum ~/Descargas/Anaconda3-2019.10-Linux-x86_64.sh

# Instala anaconda 3
bash ~/Downloads/Anaconda3-2019.10-Linux-x86_64.sh

# The installer prompts “Do you wish the installer to initialize Anaconda3 by
# running conda init?” We recommend “yes”.

# Para inicializarla manualmente
#source ${HOME}/anaconda3/bin/activate
#conda init

# Para que los cambios tengan efecto
source ~/.bashrc

# Para controlar si cada sesión de shell tiene el 'base environment' activado
#conda config --set auto_activate_base True


# Para ejecutar conda desde cualquier sitio sin tener el 'base environment'
# activado por defecto, usa
# Sólo funciona si has ejecutado 'conda init' antes
conda config --set auto_activate_base False

source ~/.bashrc
# Actualiza anaconda3
bash ~/Descargas/Anaconda3-2019.10-Linux-x86_64.sh -u
