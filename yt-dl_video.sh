#!/bin/bash

#################################################################
##   Nombre del script: Calibre-Manager.sh
##   Archivos del script: XXXXX
##   OS tested: KDE Neon, Debian testing, Ubuntu LTS
##   Propósito: Acciones básicas con Calibre
##   Autor: Jorge G.
##   Fecha creación: 25/12/2019
##   Fecha últ. revisión: 23/01/2020
#################################################################
##   ToDo List: check deps, user y set custom filenames (delete spaces)
##
#################################################################

################
# FORMATO Y ESTILO #
################

#### Color del texto
red_t=$(tput setaf 1)
green_t=$(tput setaf 2)
orange_t=$(tput setaf 3)
blue_t=$(tput setaf 27)
purple_t=$(tput setaf 5)
white_t=$(tput setaf 7)
grey_t=$(tput setaf 8)
black_t=$(tput setaf 16)
darkyellow_t=$(tput setaf 155)

#### Color del fondo
red_b=$(tput setab 1)
green_b=$(tput setab 2)

#### Formato texto
bold=$(tput bold)
uline=$(tput smul)
rst=$(tput sgr0)

#### Formato inicio mensajes
inf_s="${bold} [${green_t}+${rst}${bold}]${rst} "         # mensajes del script
qst_s="${bold} [${orange_t}?${rst}${bold}]${rst} "      # preguntas/interacción  con el usuario
err_s="${bold} [${red_t}x${rst}${bold}]${rst} "             # errores

################
####  Variables  ####
################

yt_dl_dir="${HOME}/Multimedia/Vídeos/youtube-dl"

##########
# Funciones #
##########

pausa(){
  read -p "${qst_s}Presiona ${bold}${darkyellow_t}ENTER${rst} para continuar..." fackEnterKey
}

chk_connection(){
  # Comprueba que hay conexión a Internet
  clear

  url="www.google.es"
  count=3

  printf "${inf_s}${purple_t}${bold}CHECKS${rst} previos...\n"
  printf "${inf_s}Probando la conexión..."

  if (ping -c ${count} "${url}" 1> /dev/null); then
    message="\n${inf_s}Internet Connection ${purple_t}${bold}STATUS${rst}: ${green_b}${black_t} OK ${rst}\n"
    printf "${message}"
    pausa
  else
    message="\n${err_s}Internet Connection ${purple_t}${bold}STATUS${rst}: ${red_b}${black_t} KO ${rst}\n"
    printf "${message}"
    exit
  fi
}

##############################################
################  INICIO DEL SCRIPT ################
##############################################

chk_connection
clear

if [ ! -d ${yt_dl_dir} ]; then
    mkdir -p ${yt_dl_dir}
fi

cd ${yt_dl_dir} || return

if hash aria2c 2> /dev/null; then
  youtube-dl -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]/best' --merge-output-format mp4 --external-downloader aria2c -o '%(title)s.%(ext)s' "$1"
else
  youtube-dl -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]/best' --merge-output-format mp4 -o '%(title)s.%(ext)s' "$1"
fi
